# Environment setup

#v4l2-utils: git://linuxtv.org/v4l-utils.git
v4l2_utils=
v4l2_ctl=${v4l2_utils}/utils/v4l2-ctl/v4l2-ctl
media_ctl=${v4l2_utils}/utils/media-ctl/media-ctl

#yavta: git://git.ideasonboard.org/yavta.git
yavta=

# raw2rgbpnm: git://salottisipuli.retiisi.org.uk/~sailus/raw2rgbpnm.git
r2r=

# Assume media0
# TODO identify it
mdev=/dev/media0
