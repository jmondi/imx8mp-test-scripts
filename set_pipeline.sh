#!/bin/bash -x

source ./env.sh
source ./helpers.sh

# Entity names
isi0="mxc_isi.0"
isi1="mxc_isi.1"
csi="csis-32e40000.csi"
ov5640="ov5640 1-003c"
cap0="mxc_isi.0.capture"
cap1="mxc_isi.1.e"
cap0_vdev="/dev/video0"

mc="${media_ctl} -v -d ${mdev}"

w=$1
y=$2

# YUYV
#code=UYVY
#fmt=YUYV

# RGB56
#code=RGB565_1X16
#fmt=RGB565

# RGB24
code=RGB888_1X24
fmt=RGB24

# RAW
#code=SBGGR8
#fmt=SBGGR8

size="${w}x${y}"

reset_mc ${mdev}

# Set up formats on the pipeline
${mc} -V "'${ov5640}':0[fmt:$code/$size field:none@1/15]"
${mc} -V "'${csi}':0[fmt:$code/$size field:none]"
${mc} -V "'${csi}':1[fmt:$code/$size field:none]"
${mc} -V "'${isi0}':0[fmt:$code/$size field:none]"
${mc} -V "'${isi0}':1[fmt:$code/$size field:none]"

# Enable links
${mc} -l "'${ov5640}':0 -> '${csi}':0 [1]"

${yavta} -f $fmt -s $size ${cap0_vdev}
