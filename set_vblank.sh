#!/bin/bash -x

source env.sh

# Entity names
isi0="mxc_isi.0"
isi1="mxc_isi.1"
csi="csis-32e40000.csi"
ov5640="ov5640 1-003c"
cap0="mxc_isi.0.capture"
cap1="mxc_isi.1.e"
cap0_vdev="/dev/video0"

vblank=$1
devnode="/dev/v4l-subdev3"
${v4l2_ctl} -d $devnode -c 0x009e0901=${vblank}
