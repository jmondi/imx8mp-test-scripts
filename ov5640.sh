#!/bin/bash -x

source ./env.sh

# Entity names
isi0="mxc_isi.0"
isi1="mxc_isi.1"
csi="csis-32e40000.csi"
ov5640="ov5640 1-003c"
cap0="mxc_isi.0.capture"
cap1="mxc_isi.1.e"
cap0_vdev="/dev/video0"

# ov5640 format list - just for reference, on 5.16-rc4
#	0x4001: MEDIA_BUS_FMT_JPEG_1X8
#	0x2006: MEDIA_BUS_FMT_UYVY8_2X8
#	0x200f: MEDIA_BUS_FMT_UYVY8_1X16
#	0x2008: MEDIA_BUS_FMT_YUYV8_2X8
#	0x2011: MEDIA_BUS_FMT_YUYV8_1X16
#	0x1008: MEDIA_BUS_FMT_RGB565_2X8_LE
#	0x1007: MEDIA_BUS_FMT_RGB565_2X8_BE
#	0x3001: MEDIA_BUS_FMT_SBGGR8_1X8
#	0x3013: MEDIA_BUS_FMT_SGBRG8_1X8
#	0x3002: MEDIA_BUS_FMT_SGRBG8_1X8
#	0x3014: MEDIA_BUS_FMT_SRGGB8_1X8

# For reference, media-ctl formats list
# from utils/media-ctl/libv4l2subdev.c
#{ "FIXED", MEDIA_BUS_FMT_FIXED },
#{ "Y8", MEDIA_BUS_FMT_Y8_1X8 },
#{ "Y10", MEDIA_BUS_FMT_Y10_1X10 },
#{ "Y12", MEDIA_BUS_FMT_Y12_1X12 },
#{ "YUYV", MEDIA_BUS_FMT_YUYV8_1X16 },
#{ "YUYV1_5X8", MEDIA_BUS_FMT_YUYV8_1_5X8 },
#{ "YUYV2X8", MEDIA_BUS_FMT_YUYV8_2X8 },
#{ "UYVY", MEDIA_BUS_FMT_UYVY8_1X16 },
#{ "UYVY1_5X8", MEDIA_BUS_FMT_UYVY8_1_5X8 },
#{ "UYVY2X8", MEDIA_BUS_FMT_UYVY8_2X8 },
#{ "VUY24", MEDIA_BUS_FMT_VUY8_1X24 },
#{ "SBGGR8", MEDIA_BUS_FMT_SBGGR8_1X8 },
#{ "SGBRG8", MEDIA_BUS_FMT_SGBRG8_1X8 },
#{ "SGRBG8", MEDIA_BUS_FMT_SGRBG8_1X8 },
#{ "SRGGB8", MEDIA_BUS_FMT_SRGGB8_1X8 },
#{ "SBGGR10", MEDIA_BUS_FMT_SBGGR10_1X10 },
#{ "SGBRG10", MEDIA_BUS_FMT_SGBRG10_1X10 },
#{ "SGRBG10", MEDIA_BUS_FMT_SGRBG10_1X10 },
#{ "SRGGB10", MEDIA_BUS_FMT_SRGGB10_1X10 },
#{ "SBGGR10_DPCM8", MEDIA_BUS_FMT_SBGGR10_DPCM8_1X8 },
#{ "SGBRG10_DPCM8", MEDIA_BUS_FMT_SGBRG10_DPCM8_1X8 },
#{ "SGRBG10_DPCM8", MEDIA_BUS_FMT_SGRBG10_DPCM8_1X8 },
#{ "SRGGB10_DPCM8", MEDIA_BUS_FMT_SRGGB10_DPCM8_1X8 },
#{ "SBGGR12", MEDIA_BUS_FMT_SBGGR12_1X12 },
#{ "SGBRG12", MEDIA_BUS_FMT_SGBRG12_1X12 },
#{ "SGRBG12", MEDIA_BUS_FMT_SGRBG12_1X12 },
#{ "SRGGB12", MEDIA_BUS_FMT_SRGGB12_1X12 },
#{ "AYUV32", MEDIA_BUS_FMT_AYUV8_1X32 },
#{ "RBG24", MEDIA_BUS_FMT_RBG888_1X24 },
#{ "RGB32", MEDIA_BUS_FMT_RGB888_1X32_PADHI },
#{ "ARGB32", MEDIA_BUS_FMT_ARGB8888_1X32 },

size="${w}x${y}"
mc="${media_ctl} -v -d ${mdev}"

out="./ov5640_test/"

capture_test()
{
	w=$1
	y=$2
	code=$3
	fmt=$4
	size="${w}x${y}"
	outdir=$out/$size/$fmt/

	# remove state files or create output directory
	if [ -d $outdir/ ]; then
		rm $outdir/*;
	else
		mkdir -p $outdir/
	fi

	reset_mc ${mdev}

	# Set up formats on the pipeline
	${mc} -V "'${ov5640}':0[fmt:$code/$size field:none@1/30]"
	${mc} -V "'${csi}':0[fmt:$code/$size field:none]"
	${mc} -V "'${csi}':1[fmt:$code/$size field:none]"
	${mc} -V "'${isi0}':0[fmt:$code/$size field:none]"
	${mc} -V "'${isi0}':1[fmt:$code/$size field:none]"

	# Enable links
	${mc} -l "'${ov5640}':0 -> '${csi}':0 [1]"

	devnode="/dev/v4l-subdev3"
	#hblank
	#${v4l2_ctl} -d $devnode -c 0x009e0902=960
	#vblank
	#${v4l2_ctl} -d $devnode -c 0x009e0901=1520

	${yavta} -f $fmt -s $size -c10 --skip 7 --file="$outdir/ov5640-#.bin" ${cap0_vdev}

	for f in $outdir/*bin; do
		name=$(basename $f .bin)
		${r2r} -f $fmt -s $size $outdir/$name.bin $outdir/$name.ppm
	done
}

capture_all_res()
{
	code=$1
	fmt=$2

	# RAW not supported in lower resolution
	if [[ $fmt != "SBGGR8" ]]; then
		capture_test 160 120 $code $fmt
		capture_test 176 144 $code $fmt
		capture_test 320 240 $code $fmt
		capture_test 640 480 $code $fmt
		capture_test 720 480 $code $fmt
		capture_test 720 576 $code $fmt
		capture_test 1024 768 $code $fmt
	fi

	# Full-res mode obtained by cropping do not work with 24-bpp modes
	if [[ $fmt == "RGB24" ]]; then
		return
	fi

	capture_test 1280 720  $code $fmt
	capture_test 1920 1080 $code $fmt
	capture_test 2592 1944 $code $fmt
}

capture_all_res UYVY YUYV
capture_all_res RGB565_1X16 RGB565
capture_all_res RGB888_1X24 RGB24
capture_all_res SBGGR8 SBGGR8
