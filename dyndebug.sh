#!/bin/bash

debugfs=/sys/kernel/debug
if [[ ! -d $debugfs ]]; then
	mount -t debugfs none /sys/kernel/debug
fi

control=$debugfs/dynamic_debug/control

csi=drivers/staging/media/imx/imx7-mipi-csis.c
isi_pipe=drivers/staging/media/imx/imx8-isi-pipe.c
ov5640=drivers/media/i2c/ov5640.c

echo "file $csi +p" > $control
echo "file $isi_pipe +p" > $control
echo "file $ov5640 +p" > $control
