#!/bin/bash

get_vdev() {
    name="$1"
    grep -l "$name" /sys/class/video4linux/*/name | \
	    sed 's#.*video4linux\(.*\)/name#/dev\1#g'
}

reset_mc() {
    mdev=$1
    $media_ctl -d $mdev -r
}

validate_env()
{
	if [[ ! -x ${v4l2_ctl} ]]; then
		echo ${v4l2_ctl} " not found or not executable"
		exit 1
	fi

	if [[ ! -x ${media_ctl} ]]; then
		echo ${media_ctl} " not found or not executable"
		exit 1
	fi
}

validate_env
